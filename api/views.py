from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from suds.client import Client

@api_view(['POST',])
def get_por_cedula(request):    
    if request.method == 'POST':        
        cedula = request.data.get('cedula') or None
        
        if cedula is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        response = get_data(cedula)
        if response is None:
            return Response('Not found',status=status.HTTP_404_NOT_FOUND)            

        return Response(response, status=status.HTTP_200_OK)

def get_data(cedula):
    url = 'https://wsprodu.registrocivil.gob.ec/WsRegistroCivil/ConsultaCiudadano?wsdl'
    institution = '86'
    agency = '125'
    user = 'gadmorona1'
    password = 'G@ZnY3%j'
    proxy = {'http': '172.16.8.1:3128', 'https': '172.16.8.1:3128'}
    client = Client(url, proxy=proxy)
    result = client.service.BusquedaPorNui(institution, agency, user, password, cedula)
    return result

