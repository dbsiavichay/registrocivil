from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.get_por_cedula, name='consulta_por_cedula'),
)