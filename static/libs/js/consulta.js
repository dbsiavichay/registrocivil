$(function () {
	
	$('#consultar').on('click', function () {
		var cedula = $('#inputCedula').val();
		if(!cedula) return;

		var request = $.ajax({
	        url: '/api/',        
	        method: 'POST', 
	        data: {"cedula": cedula},
	        dataType: 'json'	        
	    });

	    request.done(function (data) {	    		    	
	    	var form = $('#result').children('.form-horizontal');
	    	form.children().remove();
	    	for(attr in data) {
	    		form.prepend(getItem(attr, data[attr]));
	    	}

	    	$('#result').show();
	    });

	    request.error(function (error) {
	    	var form = $('#result').children('.form-horizontal');
	    	form.children().remove();
	    	console.log(error);
	    });
	});

	function getItem (label, value) {
		var template = '<div class="form-group form-group-sm" style="margin-bottom: 0">'+
						    '<label class="col-sm-2 control-label">'+label+':</label>'+
						    '<div class="col-sm-8">'+
						    	'<p type="text" class="form-control">'+value+'</p>'+
						    '</div>'+
						'</div>'

		return template;
	}
});
